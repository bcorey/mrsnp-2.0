#!/usr/bin/env python
# encoding: utf-8

"""
run.py is the main entry point for mrsnp
"""

__author__ = 'Brendan Corey'
__version__ = '1.3'

import subprocess
import sys

from mrsnp2.analysis.reference import build_reference
from mrsnp2.analysis.analysis import concatenate_group_alignment_files, run_gubbins
from mrsnp2.analysis.sample import analyses_precheck, threaded_analyses_preview, threaded_analyses_summary
from mrsnp2.common.miscellaneous import parse_arguments, check_required_programs, file_check
from mrsnp2.common.threading_methods import thread_handler, snp_caller_worker, clean_up_worker
from mrsnp2.common import log


#######################################################################################################################
#
# mrsnp():
#   entry point for mrsnp package.
#
#######################################################################################################################
def mrsnp():
    parse_arguments(__author__, __version__)


#######################################################################################################################
#
# run():
#   runs SNP analysis pipeline
#
#######################################################################################################################
def run(args):

    # if the pbs flag was invoked, jump to the run_pbs()
    if args.pbs:
        run_pbs(args)

    # validate input and initialize logger
    samples, groups, args, logger, reference_concatig_dict = analyses_precheck(args=args)

    # check for the required programs
    check_required_programs(args=args, logger=logger)

    # use threaded_analyses_preview to report what analyses are to be performed to the terminal/log
    samples_to_test = threaded_analyses_preview(args=args, samples=samples, groups=groups, logger=logger)

    if len(samples_to_test) > 0:

        # begin threaded analyses
        thread_handler(process=snp_caller_worker, new_samples=samples_to_test, args=args, logger=logger)

        # summarize the results of the threaded analysis
        group_status_dict = threaded_analyses_summary(samples=samples_to_test, groups=groups, logger=logger)

    # concatenate all of the .fa sequence output from all groups into a dict of multi-fasta group alignment files for groups
    # that don't have failed samples
    logger.log_section_header('Summary of Analysis Group Statuses')
    logger.log_explanation("Groups that include one or more samples that failed to successfully complete individual threaded analysis during the current run are excluded from further analysis (alignment file generation and gubbins)")
    good_groups = []
    for group in groups:
        if group in group_status_dict.keys():
            if not group_status_dict[group]:
                continue
        good_groups.append(group)

    logger.log(log.underline('The following groups will continue analysis:'))
    for group in good_groups:
        logger.log(str(group))
    
    logger.log(log.underline('\nThe following groups had one or more samples fail to complete threaded analysis and require investigation/intervention:'))
    for group in [group for group in groups if group not in good_groups]:
        logger.log(str(group))

    group_alignment_files = concatenate_group_alignment_files(groups=good_groups, args=args, reference_concatig_dict=reference_concatig_dict)

    # invoke gubbins if it was requested at the command line
    if args.gubbins:
        run_gubbins(group_alignment_files=group_alignment_files, args=args, logger=logger)

    logger.log_section_header('Cleaning up intermediate files')
    # run a post-clean based on the keep parameter
    thread_handler(process=clean_up_worker, new_samples=samples_to_test, args=args, logger=logger)


#######################################################################################################################
#
# check():
#   validates the SNP analysis run requested
#
#######################################################################################################################
def check(args):
    # validate input and initialize logger
    samples, groups, args, logger = analyses_precheck(args=args)

    # check for the required dependencies
    check_required_programs(args, logger)

    # log out the threaded analyses preview
    threaded_analyses_preview(args=args, samples=samples, groups=groups, logger=logger)

    logger.log(log.bold_green('\nCheck is now complete. Use "mrsnp run" with the same parameters to perform the outlined analyses!\n'))


#######################################################################################################################
#
# run_pbs():
#   submits the run command as a pbs batch job
#
#######################################################################################################################
def run_pbs(args):
        """
        create and submit a pbs batch script that will perform this analysis. This gets around the network stability
        issues that can cause long runnning jobs to be terminated
        """

        # run the precheck workflow to ensure that the job we submit here is valid
        args.entry_point = 'check'
        samples, groups, args, logger = analyses_precheck(args=args)

        logger.log_section_header('Submitting mrsnp2 job to pbs server')
        logger.log_explanation('Since the -pbs flag was invoked, we will submit the job to the pbs batch server for'
                                    'processing. Please note that the current script will terminate once the job has '
                                    'been submitted, so you will need to monitor qstat to evaluate when the job has '
                                    'completed\n')

        logger.log('Generating the pbs batch script\n')

        # set the pbs scipt file path
        pbs_script_file = args.output / 'mrsnp2.pbs'

        logger.log('Batch script file at: {}\n'.format(str(pbs_script_file)))

        # verify that the pbs file does NOT already exist. If it does, delete it
        if file_check(file_path=pbs_script_file, file_or_directory='file'):
            logger.log('An existing pbs batch script file was located and will be overwritten\n')
            subprocess.run(['rm', str(pbs_script_file)])
        
        node_dict = {
            '2': ['amedpbswrair002.amed.ds.army.mil', 31],
            '3': ['amedpbswrair003.amed.ds.army.mil', 31],
            '4': ['amedpbswrair004.amed.ds.army.mil', 31],
            '5': ['amedpbswrair005.amed.ds.army.mil', 39],
            '6': ['amedpbswrair006.amed.ds.army.mil', 39],
            '7': ['amedpbswrair007.amed.ds.army.mil', 39],
            '8': ['amedpbswrair008.amed.ds.army.mil', 39],
            '9': ['amedpbswrair009.amed.ds.army.mil', 31]
        }

        node = node_dict[str(args.pbs)][0]
        cores = min(args.cores, node_dict[str(args.pbs)][1])  # allow the user to restrict the number of cores used, but not exceed the limit of the selected node

        # prepare the pbs file
        with open(str(pbs_script_file), 'w') as pbs:

            # add the shebang line
            pbs.write('#!/bin/bash\n')

            # add the nodes, cores, and wall-time parameters. We will reset the cores requirement to a max of 30 if a
            # greater value was provided by the user to make it fit on any available node
            pbs.write('#PBS -l nodes={}:ppn={}\n'.format(node, cores))
            pbs.write('#PBS -l walltime=24:00:00\n')

            output_path = args.output / 'mrsnp2.stdout'
            error_path = args.output / 'mrsnp2.stderr'

            # write the stdout and stderr paths
            pbs.write('#PBS -o {}\n'.format(str(output_path)))
            pbs.write('#PBS -e {}\n'.format(str(error_path)))

            # teach pbs server about conda
            pbs.write('source /data/MRSN_Research/mrsn/miniconda/etc/profile.d/conda.sh\n')

            # activate might
            pbs.write('conda activate mrsnp2\n')

            # write the actual might command to be run as a batch job
            if args.gubbins:
                pbs.write('mrsnp2 run {} {} {} {} --gubbins --cores {} --keep {}'.format(args.output, args.reads, args.assemblies, args.input_file, args.cores, args.keep))
            else:
                pbs.write('mrsnp2 run {} {} {} {} --cores {} --keep {}'.format(args.output, args.reads, args.assemblies, args.input_file, args.cores, args.keep))

        # submit the pbs job to the batch server
        logger.log('Submitting the batch script file to the pbs batch server\n')

        subprocess.run(
            [
                'qsub',
                '-q',
                'batch',
                str(pbs_script_file)
            ]
        )

        logger.log('The job has been submitted. Please monitor qstat for job completion!')
        sys.exit()
