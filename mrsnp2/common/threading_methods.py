#!/usr/bin/env python3
# encoding: utf-8

import multiprocessing
import os

from mrsnp2.analysis.read_processing import multiprocessing_pesort, read_file_preprocessing
from mrsnp2.analysis.analysis import bowtie2, sam_to_bam, sort_bam, generate_vcf_file, \
    call_variants, apply_filters, index_variant_calls, generate_consensus, rename_fa_file
from mrsnp2.common import log
from mrsnp2.common.miscellaneous import quit_with_error, round_down


def core_allocator(samples=None, args=None, logger=None):
    # allocate the proper number of cores to hand to each thread as a function of the number of samples to be tested
    # and the number of cores provided by the user

    logger.log_section_header('Assigning core/thread allocation')

    # ensure that the core count isn't less than one and does not exceed the number of cores available on the machine
    if args.cores < 1:
        quit_with_error(logger, 'User requested a zero or negative number of CPUs.')
    if args.cores > len(os.sched_getaffinity(0)):
        quit_with_error(logger, 'User requested more cores for this analysis than are currently available.\nThe number of'
                        ' currently available cores is: ' + str(len(os.sched_getaffinity(0))))

    # two cases exists for threaded analysis:
    #   a) the number of cores exceeds the number of samples (potentially multiple cores per thread)
    #   b) the number of cores is equal to or less than the number of samples (strictly one core per thread)
    if args.cores > len(samples):
        adjusted_pool = int(round_down(int(args.cores), len(samples)))
        cores_per_thread = int(adjusted_pool / len(samples))
        return adjusted_pool, cores_per_thread
    else:
        cores_per_thread = 1
        return args.cores, cores_per_thread


def core_estimator(samples=None, logger=None):
    # estimate the proper number of cores to hand to each thread as a function of the number of samples to be tested
    # and the number of cores available on the system

    logger.log_section_header('Estimating core/thread allocation')

    available_cores =  len(os.sched_getaffinity(0))
    logger.log('\nWe have detected that your system has ' + log.bold_yellow(str(available_cores)) + ' available cores')

    # two cases exists for threaded analysis:
    #   a) the number of cores exceeds the number of samples (potentially multiple cores per thread)
    #   b) the number of cores is equal to or less than the number of samples (strictly one core per thread)
    if available_cores > len(samples):
        adjusted_pool = int(round_down(int(available_cores), len(samples)))
        cores_per_thread = int(adjusted_pool / len(samples))
    else:
        adjusted_pool = available_cores-1
        cores_per_thread = 1

    logger.log('The queued analyses are optimzed with ' + log.bold_yellow(str(adjusted_pool)) + ' cores.')
    logger.log('This will allow for ' + log.bold_yellow(str(cores_per_thread)) + ' cores to be allocated to each sample/reference pair during threaded analysis steps')
    logger.log('Fewer cores can be allocated, but analysis will take longer!\n')


def thread_handler(process, new_samples, args, logger=None):

    pool_size, cores_per_thread = core_allocator(samples=new_samples, args=args, logger=logger)

    for sample in new_samples:
        sample.cores_per_thread = cores_per_thread
        sample.keep = args.keep

    logger.log_section_header('Now starting threaded analyses')

    thread = multiprocessing.Pool(pool_size)
    thread.map(process, new_samples)
    thread.close()
    thread.join()


def snp_caller_worker(sample):

    try:
        read_file_preprocessing(sample)
    except AssertionError:
        return

    try:
        multiprocessing_pesort(sample)
    except AssertionError:
        return

    try:
        bowtie2(sample)
    except AssertionError:
        return

    try:
        sam_to_bam(sample)
    except AssertionError:
        return

    try:
        sort_bam(sample)
    except AssertionError:
        return

    try:
        generate_vcf_file(sample)
    except AssertionError:
        return

    try:
        call_variants(sample)
    except AssertionError:
        return

    try:
        apply_filters(sample)
    except AssertionError:
        return

    try:
        index_variant_calls(sample)
    except AssertionError:
        return

    try:
        generate_consensus(sample)
    except AssertionError:
        return

    try:
        rename_fa_file(sample)
    except AssertionError:
        return


def clean_up_worker(sample):
    sample.post_analysis_cleanup(keep=sample.keep)
