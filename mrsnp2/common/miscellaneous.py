#!/usr/bin/env python
# encoding: utf-8

import argparse
import subprocess
import sys
from pathlib import Path
import distutils.spawn
import mrsnp2
from mrsnp2.common import log


def parse_arguments(__author__, __version__):
    """
    capture user instructions from CLI. What options are available is defined by the passed kwarg entrypoint
    """

    ascii_art(logo='mrsnp2', author=__author__, version=__version__)

    ####################################################################################################################
    #   Being parser definition
    ####################################################################################################################

    parser = argparse.ArgumentParser(prog='mrsnp2',
                                     usage='mrsnp2 <output directory> <input file> [options]',
                                     description=log.bold_yellow('MRSN Single Nucleotide Polymorphism (SNP) Analysis Tool\n'),
                                     )

    subparsers = parser.add_subparsers(title='Available commands', help='', metavar='')

    ####################################################################################################################
    #   run
    ####################################################################################################################
    subparser_run = subparsers.add_parser('run',
                                          help='Run mrsnp analysis based on the template provided in the input file',
                                          usage='mrsnp run [options] <output_directory> <reads_directory> <assemblies directory> <input_file>',
                                          description=log.bold_yellow('command "run" validates and performs SNP analysis based on the input file'))

    subparser_run.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_run.add_argument('reads', type=str, help='path to the directory where the reads files are stored')
    subparser_run.add_argument('assemblies', type=str, help='path to the directory where the assembly(contig) files are stored')
    subparser_run.add_argument('input_file', type=str, help='path to the input file that describes the analysis to be run')

    optional_run_args = subparser_run.add_argument_group('Optional Arguments')
    optional_run_args.add_argument('--cores', type=int, default=1, help='The number of system cores to use in the analysis (max 39) [1]')
    optional_run_args.add_argument('--gubbins', action='store_true', help='Perfrom gubbins recombination filtering at the end of the run')
    optional_run_args.add_argument('--keep', type=int, default=2, choices=[0, 1, 2], help='Level of intermediate files to be retained after the run completes, 0 is the least and 2 is the most [2]')
    #optional_run_args.add_argument('--force', action='store_true', help='force overwrite of existing output [FALSE]')
    optional_run_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')
    optional_run_args.add_argument('--pbs', type=int, choices=[2, 3, 4, 5, 6, 7, 8, 9], help='run the analysis as a pbs batch job ON THE DESIGNATED NODE')

    subparser_run.set_defaults(func=mrsnp2.run.run)
    subparser_run.set_defaults(entry_point='run')

    ####################################################################################################################
    #   check
    ####################################################################################################################
    subparser_check = subparsers.add_parser('check',
                                          help='Perform validation tests to determine if the analysis described in the input file can be performed',
                                          usage='mrsnp check [options] <output_directory> <reads_directory> <assemblies directory> <input_file>',
                                          description=log.bold_yellow('command "check" will determine if the analysis '
                                                                      'in input file can be performed with the '
                                                                      'information provided. It will validate the '
                                                                      'format of the input file, then scan the '
                                                                      'input/output directories to determine if the '
                                                                      'requisite input files are present or if the '
                                                                      'analysis has already been completed. It will '
                                                                      'also scan your environment for the required '
                                                                      'dependency programs. Finally, it will present '
                                                                      'and log a summary based on the results of '
                                                                      'these tests. CHECK WILL NOT RUN THE ACTUAL ANALYSIS'))

    subparser_check.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_check.add_argument('reads', type=str, help='path to the directory where the reads files are stored')
    subparser_check.add_argument('assemblies', type=str, help='path to the directory where the assembly(contig) files are stored')
    subparser_check.add_argument('input_file', type=str, help='path to the input file that describes the analysis to be run')

    optional_check_args = subparser_check.add_argument_group('Optional Arguments')
    optional_check_args.add_argument('--gubbins', action='store_true', help='Perfrom gubbins recombination filtering at the end of the run')
    optional_check_args.add_argument('--keep', type=int, choices=[0, 1, 2], help='Level of intermediate files to be retained after the run completes, 0 is the least and 2 is the most [2]')
    #optional_check_args.add_argument('--force', action='store_true', help='force overwrite of existing output [FALSE]')
    optional_check_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')

    subparser_check.set_defaults(func=mrsnp2.run.check)
    subparser_check.set_defaults(entry_point='check')

    # TODO add in test methods
    """
    ####################################################################################################################
    #   test
    ####################################################################################################################
    subparser_test = subparsers.add_parser('test',
                                              help='run test set for mrsnp and exit',
                                              usage='mrsnp test <command>',
                                              description=log.bold_yellow('run test data for the selected mrsnp command'))

    subparser_test.add_argument('command', type=str, choices=['run', 'check'], help='command to test')
    subparser_test.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')
    subparser_test.set_defaults(func=mrsnp.testing.run_tests.test_main)

    """
    ####################################################################################################################
    #   version
    ####################################################################################################################
    subparser_version = subparsers.add_parser('version',
                                              help='Get versions and exit',
                                              usage='mrsnp version',
                                              description='returns the version of the installed cobra-kai package')

    subparser_version.set_defaults(func=mrsnp2.common.miscellaneous.get_version)
    subparser_version.set_defaults(version=__version__)

    ####################################################################################################################
    #
    # Collect args from parser
    #
    ####################################################################################################################

    args = parser.parse_args()

    # turn on debug messaging if verbosity == 2
    if hasattr(args, 'verbosity'):
        if getattr(args, 'verbosity') == 2:
            setattr(args, 'debug', True)
        else:
            setattr(args, 'debug', False)

    # launch analysis
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()


def path_clean(file_path):
    """
    sometimes users are careless. This method will use the pathLib module to convert user input into full posix path
    objects to avoid confusion downstream
    """

    return Path(str(file_path)).expanduser().resolve()


def file_extension_check(file_path=None, file_extension=None, logger=None):
    """
    verify that the file provided has the correct extension
    """
    for extension in file_extension:
        if extension in file_path.suffixes:
            return True
    return False


def file_check(file_path=None, file_or_directory=None):
    """
    checks to see if the file associated with the individual_sequencing_sample object exists in the file system at the
    specified PATH. Returns True if yes and False if no. Also return False if the attribute does not exist
    """
    if file_or_directory == 'file':
        if file_path:
            if file_path.exists():
                return True
            else:
                return False
        else:
            return False

    if file_or_directory == "directory":
        if file_path:
            if file_path.is_dir():
                return True
            else:
                return False
        else:
            return False


def check_for_dependency(program):
    """
    check_for_dependency() will use distutils.spawn to determine if the input dependency is available on the users PATH
    returns True if the dependency is located, False otherwise
    """
    if distutils.spawn.find_executable(str(program)):
        return True
    else:
        return False


def quit_with_error(logger, message):
    """
    Displays the given message and ends the program's execution.
    """
    logger.log(log.bold_red('Error: ') + message, 0, stderr=True)
    sys.exit(1)


def query_yes_no(question, default=None):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def round_down(num, divisor):
    return num - (num % divisor)


def ascii_art(logo=None, author=None, version=None, request_logo=False):
    """
    Print out the ascii art logo requested by the user
    :param logo: the name of the logo to be printed
    """

    if logo == 'mrsnp2':
        ascii_logo = log.bold_red("""
        __  __ _____                    ___  
        |  \/  |  __ \                  |__ \ 
        | \  / | |__) |___ _ __  _ __      ) |
        | |\/| |  _  // __| '_ \| '_ \    / / 
        | |  | | | \ \\\__ \ | | | |_) |  / /_ 
        |_|  |_|_|  \_\___/_| |_| .__/  |____|
                                | |           
                                |_|            
     """)
    if not request_logo:
        print(ascii_logo)
        print(log.bold_red("\tMRSN Single Nucleotide Polymorphism (SNP) Analysis Tool"))
        print(log.bold_red("\t\t" + str(author)))
        print(log.bold_red("\t\t\tv" + str(version)))
        print('\n')

    if request_logo:
        return ascii_logo


def get_version(args):

    print(log.bold('Installed version: ' + args.version))


def compression_handler(target_file, logger, compress=False, decompress=False, compression_type=None):
    # verify that exactly one action was specified
    try:
        assert (compress and not decompress) or (not compress and decompress)
    except AssertionError:
        quit_with_error(logger, 'Either compress OR decompress must be specified for compression_handler()!')
        raise

    # verify that the compression_type passed is one that is acceptable
    try:
        assert compression_type == 'gzip' or compression_type == 'bzip2' or compression_type == None
    except AssertionError:
        quit_with_error(logger, 'Either gzip or bzip2 must be specified for compression_handler()!')
        raise

    # if the file extension is '.tmp', it means we made this file during the run and it doesn't need to be decompressed
    if target_file.suffix == '.tmp':
        return target_file

    # if the compression type wasn't specified, try and guess based on the extension
    if compression_type is None:
        if target_file.suffix == '.gz':
            compression_type = 'gzip'
        elif target_file.suffix == '.bz2':
            compression_type = 'bzip2'
        else:
            if compress:
                logger.log(
                    'compression_handler() was asked to compress {} but was NOT told how, so it will use bzip2'.format(
                        str(target_file)))
                compression_type = 'bzip2'
            elif decompress:
                if target_file.suffix not in ['.fastq', '.fna', '.fasta', '.fa']:
                    quit_with_error(logger,
                                    'compression_handler() was asked to decompress {}, but is too derpy to figure out how! (Only supports .gz and .bz2)'.format(
                                        str(target_file)))
                else:
                    compression_type = 'bzip2'

    # set the actions based on the compression_type and compress vs decompress
    if compression_type == 'gzip':
        if compress:
            action = 'gzip'
        if decompress:
            action = 'gunzip'
    if compression_type == 'bzip2':
        if compress:
            action = 'bzip2'
        if decompress:
            action = 'bunzip2'

    # determine if the action requested is required (don't compress a compressed file, etc)
    if action == 'gzip':
        if target_file.suffix == '.gz':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name + '.gz')
    if action == 'gunzip':
        if target_file.suffix != '.gz':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name[:-3])
    if action == 'bzip2':
        if target_file.suffix == '.bz2':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name + '.bz2')
    if action == 'bunzip2':
        if target_file.suffix != '.bz2':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name[:-4])

    # perform the action using subprocess, then return the output_path
    compression_process = subprocess.run(
        [
            action,
            str(target_file)
        ],
        stderr=subprocess.PIPE
    )

    # check the return code
    try:
        assert compression_process.returncode == 0
    except AssertionError:
        quit_with_error(logger,
                        'compresssion_handler() returned a non-zero returncode when running {} on {}'.format(action,
                                                                                                             str(
                                                                                                                 target_file)))

    # validate that the output file exists
    try:
        assert file_check(file_path=output_path, file_or_directory='file')
    except AssertionError:
        quit_with_error(logger, 'compression_handler() failed to create the output file {}'.format(str(output_path)))

    return output_path


# def compression_handler(compress=False, decompress=False, target_file=None, compression_type=None, logger=None):
#     """
#     non-object oriented compression handling method. Either compress or decompress must be True. Method will determine
#     whether or not compression/decompression is required and if so it will use the subprocess module to perform the
#     requested operations
#     """
#     # verify that exactly one action was specified
#     try:
#         assert (compress and not decompress) or (not compress and decompress)
#     except AssertionError:
#         quit_with_error(logger, 'Either compress OR decompress must be specified for compression_handler()!')
#         raise

#     # verify that exactly one action was specified
#     try:
#         assert compression_type == 'gzip' or compression_type == 'bzip2'
#     except AssertionError:
#         quit_with_error(logger, 'Either gzip or bzip2 must be specified for compression_handler()!')
#         raise

#     # verify that file(s) were included to be worked on
#     try:
#         assert target_file
#     except AssertionError:
#         quit_with_error(logger, 'a target file must be passed to compression_handler()!')
#         raise

#     # validate the target file
#     try:
#         assert file_check(file_path=target_file, file_or_directory='file')
#     except AssertionError:
#         quit_with_error(logger,
#                         'Failed to locate target file at specified path: ' + str(target_file))

#     target_file = path_clean(file_path=target_file)

#     file_path_to_return = None

#     # handles .gz compression/decompression of files
#     if compression_type == 'gzip':
#         # gzip compression
#         if compress:
#             if not target_file.name.endswith('.gz'):
#                 subprocess.run(['gzip', str(target_file)])
#                 file_path_to_return = (str(target_file) + '.gz')
#             else:
#                 file_path_to_return = (str(target_file))
#         # gunzip decompression
#         if decompress:
#             if target_file.name.endswith('.gz'):
#                 subprocess.run(['gunzip', str(target_file)])
#                 file_path_to_return = ('.'.join(str(target_file).split('.')[:-1]))
#             else:
#                 file_path_to_return = (str(target_file))

#     # handles .bzip2 compression/decompression of files
#     if compression_type == 'bzip2':
#         # bzip2 compression
#         if compress:
#             if not target_file.name.endswith('.bz') or not target_file.name.endswith('.bz2') or not target_file.name.endswith('.bzip2'):
#                 subprocess.run(['bzip2', str(target_file)])
#                 file_path_to_return = (str(target_file) + '.bz')
#             else:
#                 file_path_to_return = (str(target_file))
#         # bunzip2 decompression
#         if decompress:
#             if target_file.name.endswith('.bz') or target_file.name.endswith('.bz2') or target_file.name.endswith('.bzip2'):
#                 subprocess.run(['bunzip2', str(target_file)])
#                 file_path_to_return = ('.'.join(str(target_file).split('.')[:-1]))
#             else:
#                 file_path_to_return = (str(target_file))

#     file_path_to_return = path_clean(file_path=file_path_to_return)

#     # validate the file path to return
#     try:
#         assert file_check(file_path=file_path_to_return, file_or_directory='file')
#     except AssertionError:
#         quit_with_error(logger,
#                         'compression_handler() failed to generate the following file: ' + str(target_file))

#     return file_path_to_return


# check_required_programs will make sure that the script is able to see all of the executables required to run
def check_required_programs(args, logger):

    if not distutils.spawn.find_executable('bowtie2-build'):
        quit_with_error(logger, 'could not find bowtie2-build')
    if not distutils.spawn.find_executable('bowtie2'):
        quit_with_error(logger, 'could not find bowtie2')
    if not distutils.spawn.find_executable('bbduk.sh'):
        quit_with_error(logger, 'could not find bbduk.sh')
    if not distutils.spawn.find_executable('repair.sh'):
        quit_with_error(logger, 'could not find repair.sh')
    if not distutils.spawn.find_executable('samtools'):
        quit_with_error(logger, 'could not find samtools')
    if not distutils.spawn.find_executable('bcftools'):
        quit_with_error(logger, 'could not find bcftools')
    if not distutils.spawn.find_executable('fuse.sh'):
        quit_with_error(logger, 'could not find fuse.sh')
    if args.gubbins:
        if not distutils.spawn.find_executable('run_gubbins.py'):
            quit_with_error(logger, 'could not find gubbins')
