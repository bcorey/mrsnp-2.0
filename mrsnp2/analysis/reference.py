#!/usr/bin/env python3
# encoding: utf-8

from collections import OrderedDict
import subprocess
from Bio import SeqIO
from mrsnp2.common.miscellaneous import quit_with_error
from mrsnp2.common import log


class UnableToBuildReferenceError(Exception):
    """Error is thrown when the reference is unable to be built"""

    def __init__(self, specific_error, helper_text=None):

        self.specific_error = specific_error
        self.helper_message = helper_text
    
    def __str__(self):
        if self.helper_message:
            return '\n{}\n\n{}'.format(log.bold_red(self.specific_error), self.helper_message)
        else:
            return '\n{}'.format(log.bold_red(self.specific_error))


####################################################################################################################
#
# build_reference()
#   call bowtie2-build to generate the reference files from trimmed (50bp from both ends of contigs) and filtered
#   (reject contigs <1000bp) concatig files for ALL REFERENCE FILES IN THE INPUT FILE saves all of the stdout to
#   reference_build_file.log
#
####################################################################################################################
def build_reference(sample):
    # ensure that the sample passed is actually a reference
    if not sample.is_reference:
        return

    sample.sample_logger.log_section_header('Building reference with bowtie2-build')

    # set paths for clean_assembly, assembly_concatig, and reference_build_file

    reference_directory = sample.group_directory / 'reference'

    if not reference_directory.is_dir():
        subprocess.run(['mkdir', str(reference_directory)])

    sample.clean_assembly = reference_directory/ (sample.sample_name + '.filtered_and_trimmed.fna')
    sample.assembly_concatig = reference_directory / (sample.sample_name + '.concatig.fna')
    sample.renamed_assembly_concatig = reference_directory / (sample.sample_name + '.renamed_concatig.fna')
    sample.reference_build_file = reference_directory / (sample.sample_name + '_reference_build.log')

    # don't waste compute doing work that is already done!
    if sample.reference_build_file.exists() and sample.renamed_assembly_concatig.exists():
        sample.sample_logger.log('The reference file for ' + sample.sample_name + ' has already been built!', verbosity=3)
        return sample.assembly_concatig

    # Trimming and Filtering of the reference assembly. Uses biopython SeqIO to read through the _454AllContigs.fna
    # file and select for contigs >=1000bp in length. Contigs that pass the filter are then trimmed by 50bp from
    # both ends The filtered and trimmed contigs are then written to a new file (sample.clean_assembly)
    if sample.clean_assembly.exists():
        sample.sample_logger.log(
            'Located a clean assembly file for this sample: {}'.format(str(sample.clean_assembly)))
    else:
        sample.sample_logger.log('Generating clean assembly file for this sample')

        filtered_sequences = OrderedDict()
        for seq_record in SeqIO.parse(str(sample.assembly_file), "fasta"):
            sequence = str(seq_record.seq).upper()
            if len(sequence) >= 1000:
                filtered_sequences[seq_record.id] = sequence[50:-50]
        with open(sample.clean_assembly, 'w') as clean_assembly:
            for key, value in filtered_sequences.items():
                clean_assembly.write(">  " + key + '\n' + value + '\n')

    # Build a concatig file for the reference if it doesn't already exist
    if sample.assembly_concatig.exists():
        sample.sample_logger.log(
            '\nLocated a concatig file for this sample: {}'.format(str(sample.assembly_concatig)))
    else:
        sample.sample_logger.log('\nGenerating a concatig file for this sample')
        sample.sample_logger.log("Reference contigs being concatenated using bbtools fuse.sh")
        fuse_command = "fuse.sh in=" + str(sample.clean_assembly) + " out=" + str(
            sample.assembly_concatig) + " pad=0 overwrite=t name=" + sample.sample_name
        sample.sample_logger.log('Fuse command: {}'.format(fuse_command))
        fuse_process = subprocess.run(
            [fuse_command],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        sample.sample_logger.log(str(fuse_process.stderr, 'utf-8'))

        # check the return code from bowtie2-build
        try:
            assert fuse_process.returncode == 0
        except AssertionError:
            if sample.assembly_concatig.exists():
                subprocess.run(['rm', str(sample.assembly_concatig)])
            raise UnableToBuildReferenceError('fuse returned a non-zero returncode when preparing the reference for {}: {}'.format(sample.sample_name, str(fuse_process.returncode)))


    # open the reference_build_file.log file associated with the reference, and then run the bowtie2-build command
    with open(sample.reference_build_file, 'w') as reference_build_file:

        reference_build_process = subprocess.run(
            ["bowtie2-build",
             str(sample.assembly_concatig),
             str(sample.group_directory / 'reference' / sample.sample_name)],
            stdout=reference_build_file,
            stderr=subprocess.PIPE
        )

        # check the return code from bowtie2-build
        try:
            assert reference_build_process.returncode == 0
        except AssertionError:
            raise UnableToBuildReferenceError('bowtie2-build returned a non-zero returncode when preparing the reference for {}: {}'.format(sample.sample_name, str(reference_build_process.returncode)))

    sample.sample_logger.log('Reference successfully prepared for ' + sample.sample_name, verbosity=0)

    # Rename the reference assembly concatig
    for seq_record in SeqIO.parse(str(sample.assembly_concatig), "fasta"):
        with open(sample.renamed_assembly_concatig, 'w') as f:
            seq_record.id = sample.sample_name + "_reference"
            seq_record.description = sample.sample_name + "_reference"
            SeqIO.write(seq_record, f, 'fasta')

    # Return the reference assembly concatig
    return sample.renamed_assembly_concatig
