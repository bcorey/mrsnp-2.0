#!/usr/bin/env python3
# encoding: utf-8
import csv
import fnmatch
import os
import subprocess
import sys
from pathlib import Path
import json

from mrsnp2.analysis.reference import build_reference, UnableToBuildReferenceError
from mrsnp2.common import log
from mrsnp2.common.miscellaneous import file_check, quit_with_error, compression_handler, path_clean, \
    file_extension_check, check_required_programs
from mrsnp2.common.threading_methods import core_estimator


class UnableToLocateRequiredFileError(Exception):
    """Error is thrown when a required file is unable to be located using autodetect methods"""

    def __init__(self, specific_error, helper_text=None):

        self.specific_error = specific_error
        self.helper_message = helper_text
    
    def __str__(self):
        if self.helper_message:
            return '\n{}\n\n{}'.format(log.bold_red(self.specific_error), self.helper_message)
        else:
            return '\n{}'.format(log.bold_red(self.specific_error))

# an object of class sample is used to store all of the file paths relevant to a single sample/reference pair
class Sample:

    def __init__(self, sample_name=None, output_directory=None, reads_directory=None, assembly_directory=None,
                 group=None, reference=None):
        self.sample_logger = None
        self.sample_name = sample_name
        self.reads_directory = reads_directory
        self.assembly_directory = assembly_directory
        self.output_directory = output_directory
        self.group = group
        self.group_directory = output_directory / group
        self.is_reference = False
        self.already_processed = False

        # determine the appropriate reference sample to use for the sample
        # if the reference is explicitly stated in the input_file, use that
        self.reference = None
        if reference != '':
            self.reference = reference

        # self.checkpoint holds the path to the JSON checkpoint file
        self.checkpoint = None

        # self.cores_per_thread tells the threaded analyses how many cores to use. Default is 1, but it will be
        # reevalutated using core_allocator() later
        self.cores_per_thread = 1

        # analysis intermediate file paths
        # raw read file paths
        self.r1_fastq = None
        self.r2_fastq = None

        # read preprocessing file paths
        self.processed_reads_directory = output_directory / 'processed_reads'
        self.r1_fastq_trimmed = self.processed_reads_directory / (self.sample_name + '.r1.trimmed.fastq')
        self.r2_fastq_trimmed = self.processed_reads_directory / (self.sample_name + '.r2.trimmed.fastq')

        # pesort file paths
        self.r1_fastq_sorted = self.processed_reads_directory / (sample_name + ".1.fastq")
        self.r2_fastq_sorted = self.processed_reads_directory / (sample_name + ".2.fastq")
        self.unpaired_fastq = self.processed_reads_directory / (sample_name + ".singles.fastq")

        # reference building file paths (attributes used for reference samples ONLY)
        self.assembly_file = None
        self.clean_assembly = None
        self.assembly_concatig = None
        self.reference_assembly_concatig_file = self.group_directory / 'reference' / (self.reference + '.concatig.fna')

        # reference building file paths (attributes used for ALL samples)
        self.reference_build_file = None

        # bowtie2/samtools/bcftools file paths
        self.sam_file = self.group_directory / (sample_name + "_" + self.group + ".sam")
        self.bam_file = self.group_directory / (sample_name + "_" + self.group + ".bam")
        self.sorted_bam_file = self.group_directory / (sample_name + "_" + self.group + ".sorted.bam")
        self.vcf_file = self.group_directory / (sample_name + "_" + self.group + ".vcf.gz")
        self.call_vcf_file = self.group_directory / (sample_name + "_" + self.group + ".call.vcf.gz")
        self.filter1_vcf_file = self.group_directory / (sample_name + "_" + self.group + ".filter1.vcf.gz")
        self.filter2_vcf_file = self.group_directory / (sample_name + "_" + self.group + ".filter2.vcf.gz")
        self.consensus_fa_file = self.group_directory / (sample_name + "_" + self.group + ".fa")
        self.renamed_consensus_file = self.group_directory / (sample_name + "_" + self.group + ".renamed_consensus.fna")

        # analysis checkpoint statuses
        self.read_preprocessing_status = None
        self.repair_status = None
        self.bowtie2_status = None
        self.sam_to_bam_status = None
        self.sort_bam_status = None
        self.generate_vcf_status = None
        self.call_variants_status = None
        self.apply_filters_status = None
        self.index_variant_calls_status = None
        self.generate_consensus_status = None
        self.rename_consensus_status = None

    def scan_for_fastq(self, directory=None):
        """
        scans the specified directory (default is output/reads/raw_reads/) for fastq files that match the name of the
        sample, then assigns their POSIX paths to the self.r1_fastq and self.r2_fastq object attributes. Finally, method
        returns True if both the r1 and r2 files were located, False if they were not
        """

        for file in os.scandir(directory):
            if (file.name.startswith(self.sample_name + "_") or file.name.startswith(
                    self.sample_name + ".")) and fnmatch.fnmatch(file, '*R1*fastq*'):
                self.r1_fastq = path_clean(file_path=file.path)
            elif (file.name.startswith(self.sample_name + "_") or file.name.startswith(
                    self.sample_name + ".")) and fnmatch.fnmatch(file, '*R2*fastq*'):
                self.r2_fastq = path_clean(file_path=file.path)

        # Error handling for samples that don't have the requisite read files
        self.sample_logger.log_section_header('Checking to see if read files are present')
        if (not self.r1_fastq or not self.r2_fastq) and not self.already_processed:
            raise UnableToLocateRequiredFileError("one or more fastq files are missing for: {}".format(self.sample_name))
        self.sample_logger.log('pass')

    def scan_for_assembly(self, directory=None):
        """
        scans the specified directory (default is output/assembly/<sample name>/) for fna file that match the name of the
        sample, then assigns the POSIX path to the self.assembly object attributes
        """

        # search the directory for files that start with the sample name and end with/include a common fasta extension
        for file in os.scandir(directory):
            if (file.name.startswith(self.sample_name + '_') or file.name.startswith(self.sample_name + '.')) and (
                    fnmatch.fnmatch(file, '*.fasta*') or fnmatch.fnmatch(file, '*.fna*')):
                self.assembly_file = Path(file.path).expanduser().resolve()
                return

        # error for missing assembly file for reference
        if self.is_reference and (not self.assembly_file and not self.reference_build_file.exists()):
            raise UnableToLocateRequiredFileError("You are missing the assembly file for the reference: {}".format(self.sample_name))
            

    def check_is_reference(self):
        self.sample_logger.log_section_header('Determining if this sample is a reference')
        if self.reference:
            if self.sample_name == self.reference:
                self.is_reference = True
                self.reference_build_file = self.group_directory / (self.sample_name + ".reference.build.log")
                self.sample_logger.log('Sample is a reference, and will be processed accordingly')
            else:
                self.sample_logger.log('Sample is NOT a reference, and will be processed accordingly')

    def guess_reference(self):
        self.sample_logger.log_section_header('Inferring reference from group')
        self.sample_logger.log_explanation(
            'The reference for this analysis was not explictly included, so we will attempt to infer it from the group assignment')
        if self.group_directory.is_dir():
            for file in os.scandir(self.group_directory):
                if file.name.endswith("reference.build.log"):
                    self.reference = file.name.split('.')[0]
                    self.reference_build_file = self.group_directory / (self.reference + 'reference_build.log')
            if not self.reference:
                quit_with_error(self.sample_logger,
                                "Missing the reference build for: " + self.group + "\nCan't perform analysis for: " + self.sample_name + ' as the group as the reference for this sample was not specified')
        else:
            quit_with_error(self.sample_logger,
                            "There doesn't appear to be a group directory for: " + self.group + "\nCheck the group ID in the input file\nIf the group is new, you will need to specify a reference for all samples in this group!")
        self.sample_logger.log('The reference is inferred to be: ' + str(self.reference))

    def make_group_directory(self, logger=None):
        # Generate group directories if they don't already exist, and initialize the group log file
        if not self.group_directory.is_dir():
            logger.log("Group " + self.group + " is a new group. Making the required directory\n")
            subprocess.run(["mkdir", self.group_directory])

    def initialize_sample_logger(self):
        # generate the sample specific log file and add it to the sample attributes

        log_directory = self.group_directory / 'logs'

        if not log_directory.is_dir():
            subprocess.run(['mkdir', str(log_directory)])

        self.sample_logger = log.Log(log_filename=log_directory / ('mrsnp_' + self.sample_name + '.log'),
                                     stdout_verbosity_level=0)
        self.sample_logger.log_section_header('Starting analysis prechecks')

    def check_for_completion(self):
        # Determine if the sample/reference combination has already been analyzed so that it will be skipped in the
        # analysis pipeline. Completion can be demonstrated in one of two ways
        #   1) a checkpoint file exists which notes that the sample has completed all of the analysis steps
        #   2) the renamed consensus fasta file is present for the sample (in the case where the checkpoint file is
        #      missing
        self.sample_logger.log_section_header(
            'Checking to see if anaylsis has already been completed for this sample/reference pair')

        try:
            self.read_checkpoint_file()
            assert self.rename_consensus_status
            if self.rename_consensus_status == 'complete' and self.renamed_consensus_file.exists():
                self.sample_logger.log(
                    'A renamed consensus fasta file (output from SNP analysis pipeline) has been detected for this '
                    'sample/reference pair. Skipping...')
                self.already_processed = True
                return
        except AssertionError:
            self.sample_logger.log('No checkpoint file was detected, will scan for the output file')

        if self.renamed_consensus_file.exists():
            self.sample_logger.log(
                'A renamed consensus fasta file (output from SNP analysis pipeline) has been detected for this '
                'sample/reference pair. Skipping...')
            self.already_processed = True
            return

        self.sample_logger.log('No output file detected, go for new analysis!')
        self.read_checkpoint_file(initialize=True)

    def sample_file_compression_handler(self, compress=False, decompress=False, file_attribute=None,
                                        compression_type=None):

        # verify that exactly one action was specified
        try:
            assert (compress and not decompress) or (not compress and decompress)
        except AssertionError:
            quit_with_error(self.sample_logger,
                            'Either compress OR decompress must be specified for compression_handler()!')
            raise

            # verify that exactly one action was specified
        try:
            assert compression_type == 'gzip' or compression_type == 'bzip2'
        except AssertionError:
            quit_with_error(self.sample_logger, 'Either gzip or bzip2 must be specified for compression_handler()!')
            raise

            # verify that file(s) were included to be worked on
        try:
            assert file_attribute
        except AssertionError:
            quit_with_error(self.sample_logger, 'a target file must be passed to compression_handler()!')
            raise

        if compress:
            self.sample_logger.log('Now compressing: ' + str(getattr(self, file_attribute)))
        elif decompress:
            self.sample_logger.log('Now decompressing: ' + str(getattr(self, file_attribute)))

        # send the file to compression_handler(), and update the attribute on return
        setattr(self, file_attribute, compression_handler(
            compress=compress,
            decompress=decompress,
            target_file=getattr(self, file_attribute),
            compression_type=compression_type,
            logger=self.sample_logger))

    def initialize_checkpoint_file(self, force=False):

        self.sample_logger.log_section_header('Initalizing checkpoint file')

        checkpoint_directory = self.group_directory / 'checkpoints'
        if not checkpoint_directory.is_dir():
            subprocess.run(['mkdir', str(checkpoint_directory)])

        checkpoint_file = checkpoint_directory / (self.sample_name + '_' + self.group + '_checkpoint.JSON')

        # check to make sure that a checkpoint file doesn't already exist
        try:
            assert not file_check(file_path=checkpoint_file, file_or_directory='file')
        except AssertionError:
            if force:
                # TODO include the force argument in the sample object and in this method
                pass
            else:
                self.sample_logger.log(
                    'Error: checkpoint file for ' + self.sample_name + ' in group ' + self.group + ' already exists!')
                raise

        # create the skeleton JSON content for this isolate
        checkpoint_object = {
            'read_preprocessing_status': None,
            'repair_status': None,
            'bowtie2_status': None,
            'sam_to_bam_status': None,
            'sort_bam_status': None,
            'generate_vcf_status': None,
            'call_variants_status': None,
            'apply_filters_status': None,
            'index_variant_calls_status': None,
            'generate_consensus_status': None,
            'rename_consensus_status': None
        }

        # write the skeleton JSON to the checkpoint file
        with open(checkpoint_file, 'w') as chkpnt_file:
            json.dump(checkpoint_object, chkpnt_file)

        # verify that the JSON file was successfully created
        try:
            assert file_check(file_path=checkpoint_file, file_or_directory='file')
        except AssertionError:
            self.sample_logger.log(
                'Error: initialize_checkpoint_file() failed to create the json checkpoint file for ' + self.sample_name + ' in group ' + self.group)
            raise

        self.checkpoint = checkpoint_file

        self.sample_logger.log(
            'Created the json checkpoint file for ' + self.sample_name + ' in group ' + self.group + ' at ' + str(
                self.checkpoint))

    def read_checkpoint_file(self, initialize=False):

        if self.checkpoint:
            checkpoint_file = self.checkpoint
        else:
            checkpoint_file = self.group_directory / 'checkpoints' / (self.sample_name + '_' + self.group + '_checkpoint.JSON')

        # check to see if the checkpoint file exists
        try:
            assert file_check(file_path=checkpoint_file, file_or_directory='file')
            self.checkpoint = checkpoint_file
        except AssertionError:
            if initialize:
                self.initialize_checkpoint_file()
            else:
                self.sample_logger.log(
                    'Checkpoint file for ' + self.sample_name + ' in group ' + self.group + ' cannot be located!')
                raise

        # attempt to read in the checkpoint file JSON
        with open(checkpoint_file) as chkpnt_file:
            checkpoint_data = json.load(chkpnt_file)

        # parse checkpoint_data to update the sample checkpoint statuses
        for key, value in checkpoint_data.items():
            setattr(self, key, value)

    def update_checkpoint_file(self, analysis_step=None, new_status=None):
        # Validate passed kwargs
        try:
            assert analysis_step is not None
        except AssertionError:
            quit_with_error(self.sample_logger, 'update_checkpoint_file() requires an analysis step be specified')

        try:
            assert analysis_step in [
                'read_preprocessing_status',
                'repair_status',
                'bowtie2_status',
                'sam_to_bam_status',
                'sort_bam_status',
                'generate_vcf_status',
                'call_variants_status',
                'apply_filters_status',
                'index_variant_calls_status',
                'generate_consensus_status',
                'rename_consensus_status'
            ]
        except AssertionError:
            quit_with_error(self.sample_logger,
                            'update_checkpoint_file() was passed an invalid analysis step: ' + analysis_step)

        try:
            assert new_status is not None
        except AssertionError:
            quit_with_error(self.sample_logger, 'update_checkpoint_file() requires a new status be specified')

        try:
            assert new_status in [
                None,
                'complete',
                'failed'
            ]
        except AssertionError:
            quit_with_error(self.sample_logger,
                            'update_checkpoint_file() was passed an invalid new status: ' + new_status)

        try:
            assert file_check(file_path=self.checkpoint, file_or_directory='file')
        except AssertionError:
            self.sample_logger.log(
                'Error: checkpoint file for ' + self.sample_name + ' in group ' + self.group + ' cannot be located!')
            raise

        # update the sample object status
        setattr(self, analysis_step, new_status)

        # attempt to read in the checkpoint file JSON
        with open(self.checkpoint, 'r') as chkpnt_file:
            checkpoint_data = json.load(chkpnt_file)

        # update the JSON entry for analysis step by setting it to new_status
        checkpoint_data[analysis_step] = new_status

        # write the new JSON information to the file
        with open(self.checkpoint, 'w') as chkpnt_file:
            json.dump(checkpoint_data, chkpnt_file)

    def post_analysis_cleanup(self, keep=2):
        # method will perform post analysis cleanup for samples THAT COMPLETED ALL ANALYSIS STEPS based on the --keep
        # level submitted by the user. Samples that failed to complete will retain their intermediate files for
        # troubleshooting and restarting the analysis at a viable checkpoint

        self.sample_logger.log_section_header(
            'Performing post analysis cleanup based on a keep value of {}'.format(str(keep)))
        # check to see if the analysis was performed, if not we can skip
        if self.already_processed:
            self.sample_logger.log('Sample was already processed, so no cleanup is necessary')
            return

        # check to see if the analysis was completed, if not we will skip
        self.read_checkpoint_file()
        if self.read_preprocessing_status != 'complete':
            self.sample_logger.log(
                'Sample did NOT make it all the way through analysis, so intermediate files will kept for troubleshooting and reanalyzing')
            return

        # based on the keep value, we will begin performing cleanup operations
        # all keep conditions will involve compressing the raw read files
        self.scan_for_fastq(directory=self.reads_directory)
        self.r1_fastq = compression_handler(target_file=self.r1_fastq, logger=self.sample_logger, compress=True, compression_type='bzip2')
        self.r2_fastq = compression_handler(target_file=self.r2_fastq, logger=self.sample_logger, compress=True, compression_type='bzip2')

        if keep == 2:
            # compress the trimmed read files
            self.r1_fastq_trimmed = compression_handler(target_file=self.r1_fastq_trimmed, logger=self.sample_logger, compress=True, compression_type='bzip2')
            self.r2_fastq_trimmed = compression_handler(target_file=self.r2_fastq_trimmed, logger=self.sample_logger, compress=True, compression_type='bzip2')

            # compress the sorted read files
            self.r1_fastq_sorted = compression_handler(target_file=self.r1_fastq_sorted, logger=self.sample_logger, compress=True, compression_type='bzip2')
            self.r2_fastq_sorted = compression_handler(target_file=self.r2_fastq_sorted, logger=self.sample_logger, compress=True, compression_type='bzip2')

        if keep == 1:
            # we will keep all but the largest files (processed reads, sam, bam)
            for file in [
                'r1_fastq_trimmed',
                'r2_fastq_trimmed',
                'r1_fastq_sorted',
                'r2_fastq_sorted',
                'sam_file',
                'bam_file',
                'sorted_bam_file'
            ]:
                subprocess.run(['rm', str(getattr(self, file))])

        if keep == 0:
            # delete all intermediate files
            for file in [
                'r1_fastq_trimmed',
                'r2_fastq_trimmed',
                'r1_fastq_sorted',
                'r2_fastq_sorted',
                'sam_file',
                'bam_file',
                'sorted_bam_file',
                'vcf_file',
                'call_vcf_file',
                'filter1_vcf_file',
                'filter2_vcf_file',
                'consensus_fa_file'
            ]:
                subprocess.run(['rm', str(getattr(self, file))])

    def reference_requirement_check(self, samples=None, entry_point=None):
        # ensure that either:
        #   the sample is a reference
        #   the reference for the sample is also in the list of sample names
        #   the samples group directory already exists
        # In the last case, either the reference build files for bowtie2 must already exist OR the assembly file required to make the reference
        # must be available, otherwise the analysis will be aborted. In either of these two cases we will return the reference assembly concatig
        # for use in the final alignment/gubbins in lieu of a self-mapping
        self.sample_logger.log_section_header('Checking to make sure the sample reference requirement is met')
        if self.is_reference:
            non_sample_reference_concatig = prepare_non_sample_reference(self.reference, self.output_directory, self.assembly_directory, self.group, entry_point)
            self.sample_logger.log('pass')
            return (non_sample_reference_concatig, self.group)
        
        if self.reference in [sample.sample_name for sample in samples]:
            self.sample_logger.log('pass')
            return None

        if self.group_directory.is_dir():
            non_sample_reference_concatig = prepare_non_sample_reference(self.reference, self.output_directory, self.assembly_directory, self.group, entry_point)
            self.sample_logger.log('pass')
            return (non_sample_reference_concatig, self.group)
            

        quit_with_error(self.sample_logger, 'The reference for this sample is not available')


####################################################################################################################
#
# analyses_precheck()
#   1) Clean paths for all input files/directories
#   2) Validate the output directory
#   3) Initialize the logger
#
####################################################################################################################
def analyses_precheck(args=None):
    # Ensure that args were passed and clean and validate the output directory
    try:
        assert args is not None
    except AssertionError:
        print(log.bold_red('ERROR: args were not passed to gather_input()!'))
        raise

    try:
        assert args.output
    except AssertionError:
        print(log.bold_red('ERROR: An output directory must be specified!'))
        raise
    args.output = path_clean(args.output)
    try:
        assert file_check(file_path=args.output, file_or_directory='directory')
    except AssertionError:
        print(log.bold_red('ERROR: Failed to locate the output directory at the specified path: ' + str(args.output)))
        raise

    # Initialize the logger
    logger = log.Log(log_filename=args.output / ('mrsnp_' + args.entry_point + '.log'),
                     stdout_verbosity_level=args.verbosity)
    logger.log_section_header('Validating command line input')
    logger.log('The output directory is: ' + log.green(str(args.output)))
    logger.log('The log file for this run is: ' + log.green(str(logger.log_file_path)))

    # Validate the input files w.r.t the entrypoint passed
    if args.entry_point in ['run', 'check']:

        # Clean and validate the input reads directory
        try:
            assert hasattr(args, 'reads')
        except AssertionError:
            quit_with_error(logger, 'An input directory for reads must be specified!')
        args.reads = path_clean(args.reads)
        try:
            assert file_check(file_path=args.reads, file_or_directory='directory')
        except AssertionError:
            quit_with_error(logger,
                            'Failed to locate the input directory for reads at the specified path: ' + str(args.reads))
        logger.log('reads directory is valid')

        # Clean and validate the input contigs directory
        try:
            assert hasattr(args, 'assemblies')
        except AssertionError:
            quit_with_error(logger, 'An input directory for assemblies must be specified!')
        args.assemblies = path_clean(args.assemblies)
        try:
            assert file_check(file_path=args.assemblies, file_or_directory='directory')
        except AssertionError:
            quit_with_error(logger, 'Failed to locate the input directory for assemblies at the specified path: ' + str(
                args.assemblies))
        logger.log('assemblies directory is valid')

        # Validate input file
        try:
            assert hasattr(args, 'input_file')
        except AssertionError:
            quit_with_error(logger, 'An input file must be specified!')

        args.input_file = path_clean(file_path=args.input_file)

        try:
            assert file_check(file_path=args.input_file, file_or_directory='file')
        except AssertionError:
            quit_with_error(logger, 'Failed to locate input file at specified path: ' + str(args.input_file))
        try:
            assert file_extension_check(file_path=args.input_file, file_extension=['.csv'])
        except AssertionError:
            quit_with_error(logger, 'Input file has the wrong extension (must be .csv)!')

        # import the input file using csv
        with open(args.input_file, newline='') as input_csv:
            input_dict = csv.DictReader(input_csv, delimiter=',', quotechar='|')

            # verify that the column headers are correct
            try:
                input_dict.fieldnames = [i.lower() for i in input_dict.fieldnames]
                assert input_dict.fieldnames == ['sample', 'group', 'reference']
            except AssertionError:
                quit_with_error(logger,
                                'The input file provided has an issue with the headers. Column headers must be "sample, group, reference"')

            # read in the sample information from the input file
            samples = []
            groups = []

            # verify that every row has at least a sample and a group specified
            for row in input_dict:
                try:
                    assert (row['sample'] != '' and row['group'] != '')
                    samples.append(Sample(sample_name=row['sample'],
                                          output_directory=args.output,
                                          reads_directory=args.reads,
                                          assembly_directory=args.assemblies,
                                          group=row['group'],
                                          reference=row['reference']
                                          )
                                   )
                    if row['group'] not in groups:
                        groups.append(row['group'])
                except AssertionError:
                    quit_with_error(logger,
                                    'Please verify that every sample in the input file has at least a corresponding group')
            logger.log('input file is valid')

    # NEW IN 1.3: reference_concatig_dict will store the group names as keys and the path to the reference concatig file as the value.
    # This will be used during the final alignment generation process to add the reference concatig to the final alignment, regardless
    # of whether or not self-mapping was performed
    reference_concatig_dict = {}
    
    # perform sample by sample precheck actions
    for sample in samples:

        # make sure that new samples have a group directory to work in
        sample.make_group_directory(logger=logger)

        # initialize the sample specific logger
        sample.initialize_sample_logger()

        # check the sample for completion. If complete no analysis is required so we move on to the next sample
        sample.check_for_completion()
        if sample.already_processed:
            continue

        # check for the requisite read files
        try:
            sample.scan_for_fastq(directory=args.reads)
        except UnableToLocateRequiredFileError as error:
            quit_with_error(sample.sample_logger, str(error))

        # determine if the sample is a reference
        sample.check_is_reference()

        # if the sample is a reference, make sure that the assembly file is present and if so build the reference
        if sample.is_reference:
            try:
                sample.scan_for_assembly(directory=args.assemblies)
            except UnableToLocateRequiredFileError as error:
                quit_with_error(sample.sample_logger, str(error))


        # if the sample has no reference specified, try and infer it from the group information
        if not sample.reference:
            sample.guess_reference()

        # check to make sure the reference requirement for this sample is met. non_sample_reference_concatig will be None unless
        # the sample IS the reference for its group OR the reference used is NOT included in the sample list for self-mapping
        try:
            reference_tuple = sample.reference_requirement_check(samples=samples, entry_point=args.entry_point)
            if reference_tuple:
                reference_concatig_dict[reference_tuple[1]] = reference_tuple[0]
        except UnableToLocateRequiredFileError as error:
            quit_with_error(sample.sample_logger, str(error))
        except UnableToBuildReferenceError as error:
            quit_with_error(sample.sample_logger, str(error))

    # if the entrypoint was check, we are return only the subset of variables required to 
    if args.entry_point == 'check':
        return samples, groups, args, logger

    return samples, groups, args, logger, reference_concatig_dict


####################################################################################################################
#
# threaded_analyses_preview()
#   provide a preview of the threaded (individual) analyses to be performed during the run
#
####################################################################################################################
def threaded_analyses_preview(args=None, samples=None, groups=None, logger=None):
    logger.log_section_header('Preview of queued analyses')
    all_samples_to_test = []

    for group in groups:

        logger.log('\n Analyses planned for group: {}'.format(log.bold_yellow(group)))
        samples_to_test = []
        complete_samples = []
        reference = ''
        for sample in samples:
            reference = sample.reference
            if sample.group == group:
                if not sample.already_processed:
                    samples_to_test.append(sample)
                    all_samples_to_test.append(sample)
                else:
                    complete_samples.append(sample)

        logger.log('\nThe reference for the selected group is: ' + log.bold_yellow(reference))
        logger.log('\nThe following samples are slated for analysis:')
        for sample in samples_to_test:
            logger.log(log.bold_yellow(sample.sample_name))
        logger.log('\nThe following samples have already completed analysis:')
        for sample in complete_samples:
            logger.log(log.bold_yellow(sample.sample_name))

    logger.log('\nIn total we will perform the threaded analyses on ' + log.bold_yellow(
        str(len(all_samples_to_test))) + ' sample/reference pairs')

    if args.entry_point == 'check':
        if len(all_samples_to_test) == 0:
            logger.log("All requested individual analyses have been completed!")
        else:
            core_estimator(samples=all_samples_to_test, logger=logger)
        return

    return all_samples_to_test


####################################################################################################################
#
# threaded_analyses_summary()
#   summarize the results of the threaded analyses
#
####################################################################################################################
def threaded_analyses_summary(samples=None, groups=None, logger=None):
    logger.log_section_header('Summary of threaded analyses')

    analysis_steps = [
        'read_preprocessing_status',
        'repair_status',
        'bowtie2_status',
        'sam_to_bam_status',
        'sort_bam_status',
        'generate_vcf_status',
        'call_variants_status',
        'apply_filters_status',
        'index_variant_calls_status',
        'generate_consensus_status',
        'rename_consensus_status'
    ]

    # NEW IN V1.3: in the event that a sample from a group failed, we will prevent that group from undergoing
    # the final alignment and gubbins analysis. To do this we will return a dict where the keys are the groups
    # and the value is True if every isolate analyzed passed all steps or False if any isolate in the group 
    # failed at least one stage of the analyses

    group_status_dict = {}

    for group in groups:

        group_status_dict[group] = True

        logger.log('\nAnalyses conducted for ' + log.bold_yellow('group ' + group))
        samples_tested = []
        reference = ''
        for sample in samples:
            if sample.group == group:
                if not sample.already_processed:
                    samples_tested.append(sample)
                    reference = sample.reference

        logger.log('\nThe reference for the selected group was: ' + log.bold_yellow(reference))
        logger.log('\nThe following samples were analyzed:')

        for sample in samples_tested:                

            logger.log(log.underline('\n' + sample.sample_name))
            sample.read_checkpoint_file()
            for analysis_step in analysis_steps:
                if getattr(sample, analysis_step) == 'complete':
                    status = log.bold_green('complete')
                elif getattr(sample, analysis_step) == 'failed':
                    group_status_dict[sample.group] = False
                    status = log.bold_red('failed')
                elif not getattr(sample, analysis_step):
                    group_status_dict[sample.group] = False
                    status = log.bold_yellow('not completed')

                logger.log('{}: {}'.format(analysis_step, status))
        
    return group_status_dict


####################################################################################################################
#
# prepare non-sample reference()
#   if a reference is NOT included as a sample, we will perform the reference prep tasks (if required).
#
#   Return the non-sample reference concatig for use in the final alignment file/gubbins in lieu of a self-mapping
#
####################################################################################################################
def prepare_non_sample_reference(reference_name, output_directory, assembly_directory, group, entry_point=None):

    reference = Sample(sample_name=reference_name, output_directory=output_directory, assembly_directory=assembly_directory,
                 group=group, reference=reference_name)

    reference.initialize_sample_logger()

    reference.check_is_reference()
    
    reference.scan_for_assembly(directory=assembly_directory)

    if entry_point == "run":

        non_sample_reference_concatig = build_reference(reference)

        return non_sample_reference_concatig
        



