#!/usr/bin/env python3
# encoding: utf-8
import subprocess
from pathlib import Path

from mrsnp2.common import log
from mrsnp2.common.miscellaneous import quit_with_error, file_check, compression_handler


####################################################################################################################
#
# read_file_preprocessing()
#   check to see if the processed, unsorted reads are present
#   check to see if the raw reads are both present
#   run bbduk to generate the processed, unsorted reads for analysis
#
####################################################################################################################
def read_file_preprocessing(sample=None):
    """
    perform adapter trimming on the raw fastq files
    """

    sample.sample_logger.log_section_header('Adapter/Quality Trimming')
    sample.sample_logger.log_explanation('Perform adapter (and light quality) trimming of Illumina read files using bbduk.')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.read_preprocessing_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that read file preprocessing has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.r1_fastq_trimmed, file_or_directory='file'):
        subprocess.run(['rm', str(sample.r1_fastq_trimmed)])
    if file_check(file_path=sample.r2_fastq_trimmed, file_or_directory='file'):
        subprocess.run(['rm', str(sample.r2_fastq_trimmed)])

    if not file_check(file_path=sample.processed_reads_directory, file_or_directory='directory'):
        mkdir_process = subprocess.run(['mkdir', str(sample.processed_reads_directory)], stderr=subprocess.PIPE)

    # verify that we can see the adapter file that should come packaged with the distribution
    adapter_file = Path(__file__).parent.parent / 'resources' / 'adapters.fa'
    try:
        assert file_check(file_path=adapter_file, file_or_directory='file')
    except AssertionError:
        sample.sample_logger.log(log.bold_red('read_file_preprocessing() unable to locate the adapter file at' + str(adapter_file)), verbosity=0)

    # check to ensure that we are dealing with non-compressed reads
    sample.r1_fastq = compression_handler(target_file=sample.r1_fastq, logger=sample.sample_logger, decompress=True)
    sample.r2_fastq = compression_handler(target_file=sample.r2_fastq, logger=sample.sample_logger, decompress=True)

    # prepare the adapter trimming cmd as a string
    adapter_trimming_cmd = \
        'bbduk.sh in1=' + str(sample.r1_fastq) + ' in2=' + str(sample.r2_fastq) + 'out1=' + \
        str(sample.r1_fastq_trimmed) + " out2=" + str(sample.r2_fastq_trimmed) + " ref=" + str(adapter_file) + \
        ' ktrim=r k=23 mink=11 hdist=1 tpe tbo qtrim=r trimq=8 mlf=50'

    # run the adapter trimming command
    bbduk_process = subprocess.run(
        ['bbduk.sh',
         '-Xmx3G',
         'in1=' + str(sample.r1_fastq),
         'in2=' + str(sample.r2_fastq),
         'out1=' + str(sample.r1_fastq_trimmed),
         'out2=' + str(sample.r2_fastq_trimmed),
         'ref=' + str(adapter_file),
         'ktrim=r',
         'k=23',
         'mink=11',
         'hdist=1',
         'tpe',
         'tbo',
         'qtrim=r',
         'trimq=8',
         'minlen=100'],
        stderr=subprocess.PIPE,
        universal_newlines=True)

    # log the bbduk stderr to the sample's log file
    bbduk_stderr = bbduk_process.stderr.split('Version')[1].splitlines()
    sample.sample_logger.log('Version ' + bbduk_stderr[0])
    for line in bbduk_stderr[1:]:
        sample.sample_logger.log(line)

    # check the return code from bbduk
    try:
        assert bbduk_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(log.bold_red('ERROR: bbduk returned a non-zero returncode for ' + sample.sample_name + ': ' + str(bbduk_process.returncode)), verbosity=0)
        raise

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='read_preprocessing_status', new_status='complete')


####################################################################################################################
#
# multiprocessing_pesort()
#   call to pesort_mrsn.pl to sort the R1 and R2 files for each sample redirect the **stderr** to a pesort.log file
#   for each sample
#
####################################################################################################################
def multiprocessing_pesort(sample):
    sample.sample_logger.log_section_header('Running repair.sh to correct order issues in paired read files')

    # use the checkpoint file to see if this step has already been completed
    sample.read_checkpoint_file()
    if sample.repair_status == 'complete':
        sample.sample_logger.log(
            'Checkpoint file indicates that repair.sh has already successfully been completed, so skipping this step!')
        return

    # if the checkpoint file isn't marked complete, we need to shred any existing output and start over
    if file_check(file_path=sample.r1_fastq_sorted, file_or_directory='file'):
        subprocess.run(['rm', str(sample.r1_fastq_sorted)])
    if file_check(file_path=sample.r2_fastq_sorted, file_or_directory='file'):
        subprocess.run(['rm', str(sample.r2_fastq_sorted)])
    if file_check(file_path=sample.unpaired_fastq, file_or_directory='file'):
        subprocess.run(['rm', str(sample.unpaired_fastq)])

    repair_command = 'repair.sh in1=' + str(sample.r1_fastq_trimmed) + \
                     ' in2=' + str(sample.r2_fastq_trimmed) + \
                     ' out1=' + str(sample.r1_fastq_sorted) + \
                     ' out2=' + str(sample.r2_fastq_sorted) + \
                     ' outs=' + str(sample.unpaired_fastq) + \
                     ' repair'

    repair_process = subprocess.run(
        [repair_command],
        stderr=subprocess.PIPE,
        shell=True,
        universal_newlines=True)

    sample.sample_logger.log(str(repair_process.stderr))

    # check the return code from repair
    try:
        assert repair_process.returncode == 0
    except AssertionError:
        sample.sample_logger.log(log.bold_red('ERROR: repair.sh returned a non-zero returncode for ' + sample.sample_name))
        sample.update_checkpoint_file(analysis_step='read_preprocessing_status', new_status='failed')
        raise

    # TODO method to remove empty read files (aka the case where no singletons are found

    # update the checkpoint file to reflect the status
    sample.update_checkpoint_file(analysis_step='repair_status', new_status='complete')