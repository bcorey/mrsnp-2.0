#!/usr/bin/env python
# encoding: utf-8

import setuptools

with open("README.md", "r") as readme:
    long_description = readme.read()

setuptools.setup(
    name="mrsnp2",
    version="1.3",
    author="Brendan Corey",
    author_email="brendan.w.corey.ctr@mail.mil",
    description="MRSN Single Nucleotide Polymorphism (SNP) Analysis Tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bcorey/mrsnp-2.0",
    license="GPLv3",
    packages=setuptools.find_packages(),
    package_data={
        "mrsnp2": ["resources/*.fa",
        ]
    },
    entry_points={'console_scripts': ['mrsnp2=mrsnp2.run:mrsnp']
                  },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires='>=3.5',
    zip_safe=False
)
