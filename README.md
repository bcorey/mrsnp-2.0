# MRsnp 2.0

A complete rebuild of the MRSN Single Nucleotide Polymorphism (SNP) identifcation pipeline

## Contents
* [Introduction](#introduction)
* [New Features](#new-features)
* [Installation](#installation)
  * [Conda Installation](#conda-installation)
* [Usage](#usage)

## Introduction

MRsnp was developed to automate the identification of SNP's between closely related isolates for the MRSN Bioinformatics team.

This script will perform SNP calling for an arbitrary number of sample groups, where each group will share a common reference. The sample/group/reference
relationship is defined by a user provided input file. This file must be a three column .csv file, where the first row is "sample,group,reference"

For each sample that is being analyzed, the forward and reverse paired end read files must be provided
If the sample is also to be used as a reference, the assembly file must also be provided.

Brief summary of the workflow:

Pre-analysis validation:

1. Script checks to make sure that all of the data necessary for the analysis outlined in the .csv file is present.
2. Build the reference files. Call bowtie2-build on the assembly file for each reference.

For each sample/reference pair (Individual threaded analyses):

1. Read Preprocessing: Reads will be quality and adapter trimmed with bbduk, then sorted to paired order using pesort.pl.
2. Use bowtie2 to generate SAM files for each sample combination
3. Use samtools to generate and sort SAM files from the bowtie2 output
4. Use bcftools to perform variant calling and generate a consensus fasta file


Post individual analysis steps:

1. Provide a summary of all of the threaded analyses that were performed
2. Generate an alignment file of all consensus fasta files
3. OPTIONAL: run gubbins to correct for high density SNP calls around recombination events

## New Features

MRsnp 2.0 has several new features that improve the user experience:
1. Logging. The run will have a log as well as each sample/reference analysis
2. Checkpointing. The threaded analysis steps are now checkpointed for troubleshooting and restarting aborted/failed analysis
3. Packaging. The program is now broken out into a more logical package structure
4. Conda-build. The program will now be deployed directly through conda as opposed to the original "heres a .yml" strategy

## Installation

This script is designed to be installed using [conda](https://www.anaconda.com/).

### Conda Installation
Install mrsnp into a new conda environment:
```
conda create --name mrsnp2 -c b.corey mrsnp2
```

